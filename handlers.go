package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type Task struct {
	Bucket string `bson:"src_bucket"`
	Object string `bson:"src_object",json:"object"`
	Buc string `bson:"dst_bucket",json:"buc"`
	Obj string `bson:"dst_object",json:"obj"`
	Scenes int32 `bson:"scenes",json:"scenes"`
	Length int32 `bson:"length",json:"length"`
	ETag string `bson:"eTag"`
	Text []string `bson:"text",json:"text"`
}

type GifInfo struct{
	Gif string `json:"gif"`
	Src string `json:"src"`
}

type GifList struct {
	Gifs []GifInfo `json:"gifs"`
}

type ObjectList struct {
	Created  int64  `json:"created"`
	Modified int64  `json:"modified"`
	Name     string `json:"name"`
	Objects  []struct {
		Created  int64  `json:"created"`
		ETag     string `json:"eTag"`
		Modified int64  `json:"modified"`
		Name     string `json:"name"`
	} `json:"objects"`
}

var suffixes  = []string{"mkv", "mp4", "mov", "avi", "m4v", "mpeg", "mpg", "wmv"}

func CreateGif(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	bucket := params["bucket"]

	all := r.URL.Query()["all"]
	if all != nil {
		url := fmt.Sprintf("%s/%s?list", sosURL, bucket)
		logger.Printf("url\t%s", url)

		hc := http.Client{}
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			logger.Fatalf("request creation failed")
			w.WriteHeader(500)
			return
		}

		resp, err := hc.Do(req)

		if err != nil {
			logger.Fatalf("GET request failed")
			w.WriteHeader(500)
			return
		}
		logger.Printf("%v", *resp)
		if resp.StatusCode != 200 {
			logger.Printf("Bucket does not exist")
			w.WriteHeader(400)
			return
		}

		objects := ObjectList{};
		err = json.NewDecoder(resp.Body).Decode(&objects)
		if err != nil {
			logger.Fatalf("Decode Failed")
			w.WriteHeader(500)
			return
		}

		for _, object := range objects.Objects {
			for _, suffix := range suffixes {
				if strings.HasSuffix(object.Name, suffix) {
					task := Task{
						Bucket: bucket,
						Object: object.Name,
						Buc:    bucket,
						Obj:    fmt.Sprintf("%s.gif", object.Name),
						Scenes: 3,
						Length: 10,
						ETag:   object.ETag,
						Text:   nil,
					}
					rabbit.PublishTask(&task)
					break
				}
			}

		}
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.Fatalf("Body read failed")
		return
	}
	var task = Task{Bucket: bucket, Object: "", Buc: bucket, Obj: "output.gif", Scenes: 3, Length: 10}
	logger.Printf("init: %v", task)

	if len(body) > 0 {
		logger.Printf("body: %s", body)
		err = json.Unmarshal(body, &task)
		if err != nil {
			logger.Printf("Decode failed:\t%s", err)
			w.WriteHeader(400)
			return
		}
	}

	if task.Object == "" {
		logger.Printf("No object to turn into gif")
		w.WriteHeader(400)
		return
	}

	logger.Printf("json: %v", task)

	url := fmt.Sprintf("%s/%s/%s", sosURL, bucket, task.Object)
	logger.Printf("url\t%s", url)

	hc := http.Client{}
	req, err := http.NewRequest("HEAD", url, nil)
	if err != nil {
		logger.Fatalf("request creation failed")
		w.WriteHeader(500)
		return
	}

	resp, err := hc.Do(req)

	if err != nil {
		logger.Fatalf("HEAD request failed")
		w.WriteHeader(500)
		return
	}
	logger.Printf("%v", *resp)
	if resp.StatusCode != 200 {
		logger.Printf("Object does not exist")
		w.WriteHeader(400)
		return
	}

	task.ETag = resp.Header.Get("Etag")

	// Object exists, so pass task to RabbitMQ
	rabbit.PublishTask(&task)
}

func ListGifs(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	bucket := params["bucket"]

	url := fmt.Sprintf("%s/%s?list", sosURL, bucket)
	logger.Printf("url\t%s", url)

	hc := http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		logger.Fatalf("request creation failed")
		w.WriteHeader(500)
		return
	}

	resp, err := hc.Do(req)

	objects := ObjectList{};
	err = json.NewDecoder(resp.Body).Decode(&objects)
	if err != nil {
		logger.Printf("Decode Failed")
		w.WriteHeader(500)
		return
	}

	var gifObjects []GifInfo

	for _, object := range objects.Objects {
		if strings.HasSuffix(object.Name, ".gif") {
			gifInfo := GifInfo{
				Gif: object.Name,
				Src: strings.TrimSuffix(object.Name, ".gif"),
			}
			gifObjects = append(gifObjects, gifInfo)
		}
	}

	objectList := GifList{Gifs: gifObjects}
	objectListJson, err := json.Marshal(objectList)
	if err != nil {
		log.Fatalf("Failed Marshal")
		w.WriteHeader(500)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	_, err = w.Write(objectListJson)
	if err != nil {
		log.Fatalf("Failed write")
		w.WriteHeader(500)
		return
	}
}

