package main

import (
	"github.com/streadway/amqp"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type RabbitConnector struct {
	connection *amqp.Connection
	channel *amqp.Channel
	queue amqp.Queue
	closeError chan *amqp.Error
}

func connectToRabbitMQ(url string) (*amqp.Connection, error) {
	var err error = nil
	for counter := 0; counter < 5; counter++{
		con, err := amqp.Dial(url)

		if err == nil {
			return con, nil
		}

		logger.Println(err)
		logger.Printf("Trying to reconnect to RabbitMQ at %s\n", url)
		time.Sleep(5 * time.Second)
	}
	return nil, err
}

func(connector *RabbitConnector) rabbitConnector(url string) {
	var rabbitErr *amqp.Error

	for {
		rabbitErr = <-connector.closeError
		if rabbitErr != nil {
			logger.Printf("Connecting to %s\n", url)

			var err error
			connector.connection, err = connectToRabbitMQ(url)
			failOnError(err, "Failed to connect to RabbitMQ")

			connector.closeError = make(chan *amqp.Error)
			connector.connection.NotifyClose(connector.closeError)

			ch, err := connector.connection.Channel()
			failOnError(err, "Failed to open channel")
			logger.Printf("RabbitMQ channel Opened")
			connector.channel = ch

			q, err := ch.QueueDeclare(
				"make_thumbnail",
				true,
				false,
				false,
				false,
				nil,
			)
			failOnError(err, "Failed to declare queue")
			logger.Printf("RabbitMQ Queue declared")
			connector.queue = q

		}
	}
}

func New(url string) *RabbitConnector {
	connector := new(RabbitConnector)

	connector.closeError = make(chan *amqp.Error)

	go connector.rabbitConnector(url)

	connector.closeError <- amqp.ErrClosed

	return connector
}

func(connector *RabbitConnector) PublishTask(task *Task) {
	data, err := bson.Marshal(task)
	if err != nil {
		logger.Printf("Bad Marshal: %v", err)
		return
	}

	err = connector.channel.Publish(
		"",
		connector.queue.Name,
		false,
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/bson",
			Body:         data,
		})
	failOnError(err, "Failed to publish a message")
	logger.Printf(" [x] Sent %s", task)
}

func(connector *RabbitConnector) Close() {
	connector.channel.Close()
	logger.Printf("RabbitMQ channel closed")
	connector.connection.Close()
	logger.Printf("RabbitMQ connection closed")
}