FROM golang:1.11-alpine

RUN apk update && \
    apk upgrade && \
    apk add git

WORKDIR /go/src/video-thumbnailer-web-controller
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["video-thumbnailer-web-controller"]