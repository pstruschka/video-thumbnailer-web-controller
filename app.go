package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func getEnv(key string, def string) string {
	val, ok := os.LookupEnv(key)
	if !ok {
		return def
	} else {
		return val
	}
}

var (
 	rabbit *RabbitConnector
	logger = log.New(os.Stderr, "", log.Lshortfile)
	rabbitHost = getEnv("RABBIT_HOST", "127.0.0.1")
	sosHost = getEnv("SOS_HOST", "127.0.0.1:5000")

	rabbitURL = fmt.Sprintf("amqp://guest:guest@%s:5672/", rabbitHost)
	sosURL = fmt.Sprintf("http://%s", sosHost)
)

func main() {
	logger.Printf("ENVIRONMENT\tRABBIT: %s\tSOS: %s", sosHost, rabbitHost)
	logger.Printf("Start\n")
	rabbit = New(rabbitURL)
	logger.Printf("Rabbit init.\n")
	defer rabbit.Close()

	router := NewRouter()

	logger.Fatal(http.ListenAndServe(":8000", router))
}