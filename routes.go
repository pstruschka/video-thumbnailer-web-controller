package main

import (
	"net/http"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	Queries     []string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"CreateGif",
		"POST",
		"/gifs/{bucket}",
		nil,
		CreateGif,
	},
	Route{
		"ListGifs",
		"GET",
		"/gifs/{bucket}",
		nil,
		ListGifs,
	},
}